const item = document.querySelector('.item')
const columns = document.querySelectorAll('.placeholder')

const onDragStart = event => {
  event.target.classList.add('drag')
  setTimeout(() => event.target.classList.add('hide'), 0)
}

const onDragEnd = event => {
  event.target.className = 'item'
}

const onDragOver = event => {
  event.preventDefault()
}

const onDrop = event => {
  event.target.append(item)
  event.target.classList.remove('hovered')
}

const onDragEnter = event => {
  event.target.classList.add('hovered')
}

const onDragLeave = event => {
  event.target.classList.remove('hovered')
}

columns.forEach((col, index) => {
  col.addEventListener('drop', onDrop)
  col.addEventListener('dragleave', onDragLeave)
  col.addEventListener('dragenter', onDragEnter)
  col.addEventListener('dragover', onDragOver)
})

item.addEventListener('dragstart', onDragStart)

item.addEventListener('dragend', onDragEnd)
